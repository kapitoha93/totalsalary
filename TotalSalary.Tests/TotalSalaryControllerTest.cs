﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Xml.Serialization;
using TotalSalary.Controllers;
using TotalSalary.Models;

namespace TotalSalary.Tests
{
    [TestClass]
    public class TotalSalaryControllerTest
    {
        [TestMethod]
        public void TotalSalaryWithCorrectData()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = 10000;
            double years = 2.5;
            double totalSalary = 25000;

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
                dynamic result = controller.ConsiderTotalSalary(model);
                Assert.AreEqual(totalSalary, result.Model.TotalSalary);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [TestMethod]
        public void TotalSalaryWithCorrectMaxData()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = 1000000000000;
            double years = 100;
            double totalSalary = 100000000000000;

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
                dynamic result = controller.ConsiderTotalSalary(model);
                Assert.AreEqual(totalSalary, result.Model.TotalSalary);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [TestMethod]
        public void FileExistsWithCorrectData()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = 10000;
            double years = 2.5;

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };
            string path = @controller._folder + model.EmployeeName + "." + model.AnnualSalary + "." + model.Years + ".xml";

            try
            {
                Validator.ValidateObject(model);
                dynamic result = controller.ConsiderTotalSalary(model);
                bool fileExists = File.Exists(path);
                Assert.IsTrue(fileExists);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [TestMethod]
        public void TestXmlDataWithCorrectData()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = 10000;
            double years = 2.5;
            double totalSalary = 25000;

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };

            controller.ConsiderTotalSalary(model);

            string path = @controller._folder + model.EmployeeName + "." + model.AnnualSalary + "." + model.Years + ".xml";
            try
            {
                Validator.ValidateObject(model);
                using (StreamReader readrer = new StreamReader(path, System.Text.Encoding.Default))
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(TotalSalaryModel));
                    TotalSalaryModel XmlModel = (TotalSalaryModel)formatter.Deserialize(readrer);
                    model.TotalSalary = totalSalary;
                    Assert.AreEqual(model, XmlModel);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [TestMethod]
        public void TotalSalaryWithEmptyEmployeeName()
        {
            TotalSalaryController controller = new TotalSalaryController();
            double annualSalary = 10000;
            double years = 2.5;
            string errorMessage = "Employee Name is empty";

            TotalSalaryModel model = new TotalSalaryModel
            {
                AnnualSalary = annualSalary,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(errorMessage, ex.InnerException.Message);
            }
        }

        [TestMethod]
        public void TotalSalaryWithIncorrectEmployeeName()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith 54";
            double annualSalary = 10000;
            double years = 2.5;
            string errorMessage = "Incorrect Employee Name";

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(errorMessage, ex.InnerException.Message);
            }
        }

        [TestMethod]
        public void TotalSalaryWithEmptyAnnualSalary()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double years = 2.5;
            string errorMessage = "Annual Salary is empty";

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(errorMessage, ex.InnerException.Message);
            }
        }

        [TestMethod]
        public void TotalSalaryWithIncorrectAnnualSalary()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = -1;
            double years = 2.5;
            string errorMessage = "Incorrect Annual Salary: min 0, max 1000000000000";

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(errorMessage, ex.InnerException.Message);
            }
        }

        [TestMethod]
        public void TotalSalaryWithEmptyYears()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = 1000;
            string errorMessage = "Years is empty";

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
            };
            try
            {
                Validator.ValidateObject(model);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(errorMessage, ex.InnerException.Message);
            }
        }

        [TestMethod]
        public void TotalSalaryWithIncorrectYears()
        {
            TotalSalaryController controller = new TotalSalaryController();
            string employeeName = "John Smith";
            double annualSalary = 1000;
            double years = -1;
            string errorMessage = "Incorrect Years: min 0, max 100";

            TotalSalaryModel model = new TotalSalaryModel
            {
                EmployeeName = employeeName,
                AnnualSalary = annualSalary,
                Years = years
            };
            try
            {
                Validator.ValidateObject(model);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(errorMessage, ex.InnerException.Message);
            }
        }
    }
}
