﻿using System.Web.Mvc;
using TotalSalary.Models;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;

namespace TotalSalary.Controllers
{
    public class TotalSalaryController : Controller
    {
        public readonly string _folder = ConfigurationManager.AppSettings["folder"];

        [HttpGet]
        public ActionResult Index()
        {
            return View(new TotalSalaryModel());
        }

        [HttpPost]
        public ActionResult ConsiderTotalSalary(TotalSalaryModel model)
        {
            if (model.EmployeeName != null)
            {
                model.EmployeeName = model.EmployeeName.Trim();
            }
            if (ModelState.IsValid)
            {
                model.TotalSalary = model.AnnualSalary * model.Years;
                var result = model.TotalSalary;
                SaveAsXml(model);
                return PartialView(model);
            }
            else
            {
                string errorMessage = string.Empty;
                foreach (var value in ModelState.Values)
                {
                    foreach (var error in value.Errors)
                    {
                        errorMessage += error.ErrorMessage + ".";
                        errorMessage = errorMessage.Trim('.') + ". ";
                    }
                }
                model.ErrorMessage = errorMessage.Trim(' ');
                return PartialView(model);
            }
        }

        private void SaveAsXml(TotalSalaryModel model)
        {
            string path = @_folder + model.EmployeeName + "." + model.AnnualSalary + "." + model.Years + ".xml";
            XmlSerializer formatter = new XmlSerializer(typeof(TotalSalaryModel));
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, model);
            }
        }
    }
}