﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TotalSalary.Models
{
    [Serializable]
    public class TotalSalaryModel
    {
        [Required(ErrorMessage = "Employee Name is empty")]
        [RegularExpression(@"^[A-Za-z]{1}[A-Za-z\s]+$", ErrorMessage = "Incorrect Employee Name")]
        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "Annual Salary is empty")]
        [Range(typeof(double), "0", "1000000000000", ErrorMessage = "Incorrect Annual Salary: min 0, max 1000000000000")]
        public double AnnualSalary { get; set; }

        [Required(ErrorMessage = "Years is empty")]
        [Range(typeof(double), "0", "100", ErrorMessage = "Incorrect Years: min 0, max 100")]
        public double Years { get; set; }

        public double TotalSalary { get; set; }
        public string ErrorMessage { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            TotalSalaryModel model = (TotalSalaryModel)obj;
            return (model.EmployeeName == this.EmployeeName)
                && (model.AnnualSalary == this.AnnualSalary)
                && (model.Years == this.Years)
                && (model.TotalSalary == this.TotalSalary)
                && (model.ErrorMessage == this.ErrorMessage);
        }

        public override int GetHashCode()
        {
            int hashcode = EmployeeName.GetHashCode();
            hashcode = 31 * hashcode + AnnualSalary.GetHashCode();
            hashcode = 31 * hashcode + Years.GetHashCode();
            return hashcode;
        }
    }
}